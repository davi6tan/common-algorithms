package implementstrstr1;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by davidtan on 10/27/16.
 */
public class Solution1 {

    public String findNeedle(Object[] haystack) {
        return "found the needle at position " + Arrays.asList(haystack).indexOf("needle");
    }

    public static void main(String[] args) {
        Object[] haystack = {"hello", 1, 3, "needle", new ArrayList<>()};
        Object[] haystack2 = {"hello", 1, 3, "eedle", new ArrayList<>()};
        Solution1 sol = new Solution1();
        System.out.println(sol.findNeedle(haystack));//found the needle at position 3
        System.out.println(sol.findNeedle(haystack2));//found the needle at position -1
        /////////////////

    }
}
